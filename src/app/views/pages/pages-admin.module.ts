import { NgModule }                from '@angular/core';
import { EditComponent }           from './item/edit/edit.component';
import { SharedModule }            from '../../shared/shared.module';
import { RouterModule, Routes }    from '@angular/router';
import { PagesContainerComponent } from './pages-container.component';
import { ViewComponent }           from './item/view/view.component';

const routes: Routes = [
  {
    path: '', component: PagesContainerComponent,
    children: [
      {path: '', component: ViewComponent},
      {path: 'edit', component: EditComponent}
    ]
  }
];

@NgModule({
  declarations: [
    EditComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PagesAdminModule {
}

