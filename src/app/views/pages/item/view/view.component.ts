import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-item-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent {

  constructor() {
  }
}
