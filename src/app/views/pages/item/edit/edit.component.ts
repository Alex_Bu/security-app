import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-item-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {

  constructor() {
    console.log('HEY_I_AM_EDIT_COMPONENT');
  }
}
