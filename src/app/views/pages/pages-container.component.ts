import { Component } from '@angular/core';

@Component({
  selector: 'app-pages-container',
  template: `
    <header>
      <nav class="navbar bg-dark">
        <div class="container">
          <a href="#" class="btn btn-outline-primary text-white">
            Выйти
          </a>
        </div>
      </nav>
    </header>
    <router-outlet></router-outlet>`
})
export class PagesContainerComponent {

}
