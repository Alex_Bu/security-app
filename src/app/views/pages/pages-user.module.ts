import { NgModule }                from '@angular/core';
import { SharedModule }            from '../../shared/shared.module';
import { RouterModule, Routes }    from '@angular/router';
import { PagesContainerComponent } from './pages-container.component';
import { ViewComponent }           from './item/view/view.component';

const routes: Routes = [
  {
    path: '', component: PagesContainerComponent,
    children: [
      {path: '', component: ViewComponent}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PagesUserModule {
}

