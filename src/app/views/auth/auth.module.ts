import { NgModule }                         from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule }                     from '@angular/common';
import { NgSelectModule }                   from '@ng-select/ng-select';
import { LoginComponent }                   from './login/login.component';
import { AuthRoutingModule }                from './auth-routing.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    AuthRoutingModule]
})
export class AuthModule {
}

