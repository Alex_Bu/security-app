import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormControl, Validators }            from '@angular/forms';
import { Router }                             from '@angular/router';

@Component({
  selector: 'app-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  takeRoleControl: FormControl = new FormControl('', Validators.required);
  rolesList = ['User', 'Admin'];

  constructor(
    private router: Router
  ) {
  }

  onSubmit() {
    const modulePath = this.takeRoleControl.value === 'Admin' ? 'admin' : 'user';
    this.router.navigate([modulePath]);
  }
}
