import { NgModule }                from '@angular/core';
import { RouterModule }            from '@angular/router';
import { ViewComponent }           from '../views/pages/item/view/view.component';
import { PagesContainerComponent } from '../views/pages/pages-container.component';

@NgModule({
  declarations: [
    ViewComponent,
    PagesContainerComponent
  ],
  imports: [
    RouterModule
  ]
})
export class SharedModule {
}

