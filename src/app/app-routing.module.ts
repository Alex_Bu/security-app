import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: './views/auth/auth.module#AuthModule'},
  {path: 'user', loadChildren: './views/pages/pages-user.module#PagesUserModule'},
  {path: 'admin', loadChildren: './views/pages/pages-admin.module#PagesAdminModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
